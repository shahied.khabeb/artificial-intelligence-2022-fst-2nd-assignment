### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# ╔═╡ 2265fb40-bd68-11ec-3676-cd69a57a8d8b
using Pkg

# ╔═╡ f8e439dd-703f-45ed-a3a9-4aecd72a2fe5
using DataFrames

# ╔═╡ 91b5a4af-5d17-4723-87d9-7d02782ba41b
using TypedTables

# ╔═╡ d8408877-33af-4915-96cd-23499f78f849
using PlutoUI

# ╔═╡ c495b695-d44b-4b4c-a382-e7b570594e61
using HypertextLiteral

# ╔═╡ 5da7b5fa-c996-437e-86cc-0022a08ef561
using DataStructures 

# ╔═╡ ae913fb8-3946-4943-9621-0350fc7a1570
md"# Assignment 2"

# ╔═╡ 6e4356af-f0a2-4bcd-bff7-620b5d951dd7
md"# Problem 1 "

# ╔═╡ d001f34c-21f5-4b97-b4b1-87dfb3489bd2
md" Mark: 45

Consider a multi-storey building that serves as the office for Company X.
Consider an agent in charge of the logistics in the company. Once a week,
the agent collects parcels from all offices in the building and sends them to
their destination. Each floor has a number of contiguous offices, each one
with a specific room number.

The possible actions the agent can execute are defined as follows:

move east: (me), to move to the next office eastward;

move west: (mw), to move to the next office westward;

move up:   (mu), to move to the floor up;

move down: (md), to move to the floor down;

collect:   (co), to collect a single item from an office.

Additionally, note that the agent can only collect one item at a time and can
do so only if it is in the same office where the item is situated. Furthermore,
moving eastward (or westward) is possible if the destination office exists. In
the same vein, moving up or down is possible only if the destination floor
exists. Finally, the cost attached to each action is defined in Table 1.

Your task is to write a program in Julia that implements a search-based
problem-solving mechanism using the A
∗ algorithm as a strategy. As a
heuristic function, you will apply 3 unit costs for each parcel not yet collected. A goal state is one where all parcels have been collected.

Your program will allow the user to provide a detailed configuration of the
problem at runtime. This includes the number of storeys, how many occupied offices per floor, the number of parcels in each office and the location
of the agent at the beginning.

Table 1

me = 2

mw = 2

mu = 1

md = 1

co = 5
"

# ╔═╡ c0d215ec-fa80-4f49-a0ae-00ea0de41341
struct Action
    Name::String
	Cost::Int64
end

# ╔═╡ 6bd4eb20-fe22-4cca-8756-ac00a0168358
struct Building 
	Case::String
	Parcels::Vector{Int64}
	Positions::Int64
end

# ╔═╡ f0d58b9a-2b77-4bf7-85ed-0a9531257f46
md"Implementation of action"

# ╔═╡ 45266713-88c5-410e-bf56-3b38c07076b8
me = Action("move east",2)

# ╔═╡ fa5bf707-d169-4d1d-8a63-5c04c67d0c53
mw = Action("move west",2)

# ╔═╡ 3eea3fee-91b1-4ace-9a22-65ac10088491
mu = Action("move up",1)

# ╔═╡ 58d725c2-862d-42da-a963-75d3706fa794
md = Action("move down",1)

# ╔═╡ 78160b18-8197-4056-827a-d5d43c871b22
co = Action("collect",5)

# ╔═╡ 03a7ff61-497b-46c1-9c1c-aca0c6f0b662
md"# User Configuration"

# ╔═╡ b8b606d1-86a9-42b3-a892-eeec26bdb0c8
CASENO = Building("CASENO", [1 ,3 ,2 , 1], 1)

# ╔═╡ b7783136-eade-4172-9baf-ae9b46028ba3
CASE00 = Building("CASE00",[0 ,3 ,2 ,1], 2)

# ╔═╡ 33109860-cd81-4ae9-a9ce-e7a14da75254
CASE01 = Building("CASE01",[0 ,3 ,2 ,1], 4)

# ╔═╡ 73c7b1d9-389b-4d10-9bd8-4152693128e7
CASE02 = Building("CASE02",[0 ,3 ,2 ,1], 0)

# ╔═╡ 96263d1d-0415-4a52-97a7-8876f125726d
CASE03 = Building("CASE03",[0 ,1 ,2 ,1], 0)

# ╔═╡ 2dd8f623-7db7-478f-bafc-a91705100489
CASE04 = Building("CASE04",[0 ,1 ,2 ,1], 5)

# ╔═╡ 76d5dead-1d94-4ce9-ba1a-3c357387ced2
CASE05 = Building("CASE05",[0 ,1 ,2 ,1], 2)

# ╔═╡ 1782d261-2391-452f-8ead-00d7e60a49ab
CASE06 = Building("CASE06",[0, 1 ,2 ,1], 0)

# ╔═╡ 9649db6a-5812-4eb8-9e5e-07940003a4b4
CASE07 = Building("CASE07",[0 ,0 ,0 ,2], 2)

# ╔═╡ 48eb8fa1-aed7-4961-8813-8a9f1cf71159
CASE08 = Building("CASE08",[0 ,0 ,0 ,2], 5)

# ╔═╡ 86794e17-4486-40a2-bd9e-abfdaa82055f
CASE09 = Building("CASE09",[0 ,0 ,0 ,2], 3)

# ╔═╡ dccc19ca-5153-41ca-8177-af9413059f1f
CASE10 = Building("CASE10",[0 ,0 ,0 ,2], 2)

# ╔═╡ 3da27460-c5b2-48ad-8d8a-b789cda98aed
CASE11 = Building("CASE11",[0 ,0 ,1 ,1], 2)

# ╔═╡ 7b0f4037-2894-4fa3-a807-0c67ad2bf6fc
CASE12 = Building("CASE12",[0 ,0 ,1 ,1], 3)

# ╔═╡ 67ef8773-bda3-4640-917a-eb9f0b6a43c3
CASE13 = Building("CASE13",[0 ,0 ,1 ,1], 0)

# ╔═╡ 222b62b6-8aec-4916-84fc-8c85ecd52c08
CASE14 = Building("CASE14",[0 ,0 ,1 , 1], 2)

# ╔═╡ c670e598-65b6-45e0-b983-44696a964578
CASE15 = Building("CASE15",[1 ,2 ,2 ,1], 2)

# ╔═╡ c320d70b-6ec9-434a-9094-54bf229c5efb
CASE16 = Building("CASE16",[1 ,2 ,2 ,1], 3)

# ╔═╡ 47df7f5a-ba11-4f74-80db-ba94cfa2010c
CASE17 = Building("CASE17",[1 ,2 ,2 ,1], 0)

# ╔═╡ b35a769d-d5da-44e0-9cbc-03a2c2b9ded8
CASE18 = Building("CASE18",[1 ,2 ,2 ,1], 2)

# ╔═╡ 54626637-0563-46af-83d2-63fc9234c6ab
CASE19 = Building("CASE19",[0 ,3 , 1 ,0], 4)

# ╔═╡ 75af0856-507b-4e02-8d3c-d58a80449283
CASE20 = Building("CASE20",[0 ,3 ,1 ,0], 2)

# ╔═╡ d4dae7df-7ce9-47fd-8827-c83075b021c3
md"# Transition model"

# ╔═╡ 5aacc176-094f-42ce-847f-bfe99054c542
TransitionModel = Dict(CASE02 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE02) ])

# ╔═╡ 5efcddc5-6d27-45ac-825e-56616091d645
push!(TransitionModel, CASENO => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASENO) ])

# ╔═╡ 03b13e1f-2a9e-4ac2-b32a-7ba9c75ffcca
push!(TransitionModel, CASE01 => [(me, CASE01),(mw, CASE01),(mu, CASENO),(md ,CASENO),(co ,CASE01) ])

# ╔═╡ b6c6ba81-9f05-4f43-8247-2f562cd99a03
push!(TransitionModel, CASE02 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE02) ])

# ╔═╡ 8f85b903-c9aa-4e7b-b9e7-25548ef8f7ff
push!(TransitionModel, CASE03 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE03) ])

# ╔═╡ 97e1e2de-e4c2-4fc3-a000-33537661e74a
push!(TransitionModel, CASE04 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE04) ])

# ╔═╡ ccc2e0da-adb1-4cd8-8f5c-59cc28005581
push!(TransitionModel, CASE05 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE05) ])

# ╔═╡ b6bc2479-cdae-48c0-8a9e-2a4a56b1dcdb
push!(TransitionModel, CASE06 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE06) ])

# ╔═╡ bf34e8f0-a31d-4abe-b708-b2d2450ffe96
push!(TransitionModel, CASE07 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE07) ])

# ╔═╡ 58959e6a-6ca4-47c6-b921-59308da1ddb1
push!(TransitionModel, CASE08 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE08) ])

# ╔═╡ 681cd2df-8321-400e-a8ab-f0f31490d55f
push!(TransitionModel, CASE09 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE09) ])

# ╔═╡ 10097996-b435-4b51-8cc9-20be73977147
push!(TransitionModel, CASE10 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE10) ])

# ╔═╡ f8793e0f-5941-4de8-af50-dbafda0610a9
push!(TransitionModel, CASE11 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE11) ])

# ╔═╡ 97fbd945-e6fa-4616-b2f8-df6df9bcef5d
push!(TransitionModel, CASE12 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE12) ])

# ╔═╡ f90ff8cd-8106-4ef5-be6a-767b6e710fd9
push!(TransitionModel, CASE13 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE13) ])

# ╔═╡ 0dd921ba-8731-427d-b081-e4fff35a4f19
push!(TransitionModel, CASE14 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE14) ])

# ╔═╡ 3928ddaa-3a84-4a2d-af52-0d591ef8c290
push!(TransitionModel, CASE15 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE15) ])

# ╔═╡ 79ac58a4-8d1a-4832-ac2c-bd5e0ff723ff
push!(TransitionModel, CASE16 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE16) ])

# ╔═╡ 4451cd2b-2406-4ecd-b552-21739b55d499
push!(TransitionModel, CASE17 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE17) ])

# ╔═╡ d21a251c-d784-4a7e-87a3-d289f465aca6
push!(TransitionModel, CASE18 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE18) ])

# ╔═╡ 728dd2ac-37ea-4d5a-b33d-bcc8e640b72f
push!(TransitionModel, CASE19 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE19) ])

# ╔═╡ b630190c-0e29-4e74-af9a-20cecb5c7ab2
push!(TransitionModel, CASE20 => [(me, CASENO),(mw, CASENO),(mu, CASENO),(md ,CASENO),(co ,CASE20) ])

# ╔═╡ d11b1d19-4e29-41b5-8134-4afe52b227db
md"# A* Search"

# ╔═╡ a9fead3c-a26b-4a60-83ac-ad9ec97719aa
function SearchAlgorithm(Cost)
	x = Building(Initial, nothing, nothing)
	if goal(Action.state)
		return(node, "Goal is completed", [])
	end
	
	if false
			return invalid("Path is not found")
	end
	end
		
		

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataFrames = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
HypertextLiteral = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
Pkg = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
TypedTables = "9d95f2ec-7b3d-5a63-8d20-e2491e220bb9"

[compat]
DataFrames = "~1.3.4"
DataStructures = "~0.18.11"
HypertextLiteral = "~0.9.3"
PlutoUI = "~0.7.38"
TypedTables = "~1.4.0"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.Adapt]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "af92965fb30777147966f58acb05da51c5616b5f"
uuid = "79e6a3ab-5dfb-504d-930d-738a2a938a0e"
version = "3.3.3"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.DataAPI]]
git-tree-sha1 = "fb5f5316dd3fd4c5e7c30a24d50643b73e37cd40"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.10.0"

[[deps.DataFrames]]
deps = ["Compat", "DataAPI", "Future", "InvertedIndices", "IteratorInterfaceExtensions", "LinearAlgebra", "Markdown", "Missings", "PooledArrays", "PrettyTables", "Printf", "REPL", "Reexport", "SortingAlgorithms", "Statistics", "TableTraits", "Tables", "Unicode"]
git-tree-sha1 = "daa21eb85147f72e41f6352a57fccea377e310a9"
uuid = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
version = "1.3.4"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Dictionaries]]
deps = ["Indexing", "Random"]
git-tree-sha1 = "0340cee29e3456a7de968736ceeb705d591875a2"
uuid = "85a47980-9c8c-11e8-2b9f-f7ca1fa99fb4"
version = "0.3.20"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Formatting]]
deps = ["Printf"]
git-tree-sha1 = "8339d61043228fdd3eb658d86c926cb282ae72a8"
uuid = "59287772-0a20-5a39-b81b-1366585eb4c0"
version = "0.4.2"

[[deps.Future]]
deps = ["Random"]
uuid = "9fa8497b-333b-5362-9e8d-4d0656e87820"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.Indexing]]
git-tree-sha1 = "ce1566720fd6b19ff3411404d4b977acd4814f9f"
uuid = "313cdc1a-70c2-5d6a-ae34-0150d3930a38"
version = "1.1.1"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.InvertedIndices]]
git-tree-sha1 = "bee5f1ef5bf65df56bdd2e40447590b272a5471f"
uuid = "41ab1584-1d38-5bbf-9106-f11c6c58b48f"
version = "1.1.0"

[[deps.IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "bf210ce90b6c9eed32d25dbcae1ebc565df2687f"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.0.2"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.PooledArrays]]
deps = ["DataAPI", "Future"]
git-tree-sha1 = "a6062fe4063cdafe78f4a0a81cfffb89721b30e7"
uuid = "2dfb63ee-cc39-5dd5-95bd-886bf059d720"
version = "1.4.2"

[[deps.PrettyTables]]
deps = ["Crayons", "Formatting", "Markdown", "Reexport", "Tables"]
git-tree-sha1 = "dfb54c4e414caa595a1f2ed759b160f5a3ddcba5"
uuid = "08abe8d2-0d0c-5749-adfa-8a2ac140af0d"
version = "1.3.1"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "b3363d7460f7d098ca0912c69b082f75625d7508"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.0.1"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.SplitApplyCombine]]
deps = ["Dictionaries", "Indexing"]
git-tree-sha1 = "35efd62f6f8d9142052d9c7a84e35cd1f9d2db29"
uuid = "03a91e81-4c3e-53e1-a0a4-9c0c8f19dd66"
version = "1.2.1"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[deps.Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "LinearAlgebra", "OrderedCollections", "TableTraits", "Test"]
git-tree-sha1 = "5ce79ce186cc678bbb5c5681ca3379d1ddae11a1"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.7.0"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.TypedTables]]
deps = ["Adapt", "Dictionaries", "Indexing", "SplitApplyCombine", "Tables", "Unicode"]
git-tree-sha1 = "f91a10d0132310a31bc4f8d0d29ce052536bd7d7"
uuid = "9d95f2ec-7b3d-5a63-8d20-e2491e220bb9"
version = "1.4.0"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═ae913fb8-3946-4943-9621-0350fc7a1570
# ╠═6e4356af-f0a2-4bcd-bff7-620b5d951dd7
# ╠═d001f34c-21f5-4b97-b4b1-87dfb3489bd2
# ╠═f8e439dd-703f-45ed-a3a9-4aecd72a2fe5
# ╠═91b5a4af-5d17-4723-87d9-7d02782ba41b
# ╠═d8408877-33af-4915-96cd-23499f78f849
# ╠═c495b695-d44b-4b4c-a382-e7b570594e61
# ╠═2265fb40-bd68-11ec-3676-cd69a57a8d8b
# ╠═5da7b5fa-c996-437e-86cc-0022a08ef561
# ╠═c0d215ec-fa80-4f49-a0ae-00ea0de41341
# ╠═6bd4eb20-fe22-4cca-8756-ac00a0168358
# ╠═f0d58b9a-2b77-4bf7-85ed-0a9531257f46
# ╠═45266713-88c5-410e-bf56-3b38c07076b8
# ╠═fa5bf707-d169-4d1d-8a63-5c04c67d0c53
# ╠═3eea3fee-91b1-4ace-9a22-65ac10088491
# ╠═58d725c2-862d-42da-a963-75d3706fa794
# ╠═78160b18-8197-4056-827a-d5d43c871b22
# ╠═03a7ff61-497b-46c1-9c1c-aca0c6f0b662
# ╠═b8b606d1-86a9-42b3-a892-eeec26bdb0c8
# ╠═b7783136-eade-4172-9baf-ae9b46028ba3
# ╠═33109860-cd81-4ae9-a9ce-e7a14da75254
# ╠═73c7b1d9-389b-4d10-9bd8-4152693128e7
# ╠═96263d1d-0415-4a52-97a7-8876f125726d
# ╠═2dd8f623-7db7-478f-bafc-a91705100489
# ╠═76d5dead-1d94-4ce9-ba1a-3c357387ced2
# ╠═1782d261-2391-452f-8ead-00d7e60a49ab
# ╠═9649db6a-5812-4eb8-9e5e-07940003a4b4
# ╠═48eb8fa1-aed7-4961-8813-8a9f1cf71159
# ╠═86794e17-4486-40a2-bd9e-abfdaa82055f
# ╠═dccc19ca-5153-41ca-8177-af9413059f1f
# ╠═3da27460-c5b2-48ad-8d8a-b789cda98aed
# ╠═7b0f4037-2894-4fa3-a807-0c67ad2bf6fc
# ╠═67ef8773-bda3-4640-917a-eb9f0b6a43c3
# ╠═222b62b6-8aec-4916-84fc-8c85ecd52c08
# ╠═c670e598-65b6-45e0-b983-44696a964578
# ╠═c320d70b-6ec9-434a-9094-54bf229c5efb
# ╠═47df7f5a-ba11-4f74-80db-ba94cfa2010c
# ╠═b35a769d-d5da-44e0-9cbc-03a2c2b9ded8
# ╠═54626637-0563-46af-83d2-63fc9234c6ab
# ╠═75af0856-507b-4e02-8d3c-d58a80449283
# ╠═d4dae7df-7ce9-47fd-8827-c83075b021c3
# ╠═5aacc176-094f-42ce-847f-bfe99054c542
# ╠═5efcddc5-6d27-45ac-825e-56616091d645
# ╠═03b13e1f-2a9e-4ac2-b32a-7ba9c75ffcca
# ╠═b6c6ba81-9f05-4f43-8247-2f562cd99a03
# ╠═8f85b903-c9aa-4e7b-b9e7-25548ef8f7ff
# ╠═97e1e2de-e4c2-4fc3-a000-33537661e74a
# ╠═ccc2e0da-adb1-4cd8-8f5c-59cc28005581
# ╠═b6bc2479-cdae-48c0-8a9e-2a4a56b1dcdb
# ╠═bf34e8f0-a31d-4abe-b708-b2d2450ffe96
# ╠═58959e6a-6ca4-47c6-b921-59308da1ddb1
# ╠═681cd2df-8321-400e-a8ab-f0f31490d55f
# ╠═10097996-b435-4b51-8cc9-20be73977147
# ╠═f8793e0f-5941-4de8-af50-dbafda0610a9
# ╠═97fbd945-e6fa-4616-b2f8-df6df9bcef5d
# ╠═f90ff8cd-8106-4ef5-be6a-767b6e710fd9
# ╠═0dd921ba-8731-427d-b081-e4fff35a4f19
# ╠═3928ddaa-3a84-4a2d-af52-0d591ef8c290
# ╠═79ac58a4-8d1a-4832-ac2c-bd5e0ff723ff
# ╠═4451cd2b-2406-4ecd-b552-21739b55d499
# ╠═d21a251c-d784-4a7e-87a3-d289f465aca6
# ╠═728dd2ac-37ea-4d5a-b33d-bcc8e640b72f
# ╠═b630190c-0e29-4e74-af9a-20cecb5c7ab2
# ╠═d11b1d19-4e29-41b5-8134-4afe52b227db
# ╠═a9fead3c-a26b-4a60-83ac-ad9ec97719aa
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
